//
//  ViewController.swift
//  CoreMLCustomModel
//
//  Created by Oleg Rahouski on 9/3/18.
//  Copyright © 2018 Oleg Rahouski. All rights reserved.
//

import UIKit
import CoreML
import Vision

final class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imgGuess: UIImageView!
    @IBOutlet weak var lblGuess: UILabel!

    @IBAction func takePhoto(_ sender: UIButton) {

        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    //MARK: - UIImagePickerControllerDelegate

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {

            lblGuess.text = "Processing..."

            imgGuess.contentMode = .scaleToFill
            imgGuess.image = pickedImage

            //get the model
            guard let model = try? VNCoreMLModel(for: people().model) else {
                fatalError("Unable to load model")
            }

            //create vision request
            let request = VNCoreMLRequest(model: model) { [weak self] request, error in

                guard let results = request.results as? [VNClassificationObservation], let topResult = results.first else {
                    fatalError("Unexpected results")
                }

                DispatchQueue.main.async { [weak self] in
                    self?.lblGuess.text = "\(topResult.identifier) with \(Int(topResult.confidence * 100))% confidence"
                }
            }

            guard let ciImage = CIImage(image: pickedImage) else {
                fatalError("Cannot read picked image")
            }

            //run the classifier
            let handler = VNImageRequestHandler(ciImage: ciImage, options: [:])
            DispatchQueue.global().async {
                do {
                    try handler.perform([request])
                }
                catch {
                    print(error)
                }
            }
        }

        picker.dismiss(animated: true, completion: nil)
    }
}









